$("#image_resizer_form").on('submit', function(){
    event.preventDefault();

    var file = $("input[name='selected_image']")[0].files[0]

    var formData = new FormData()
    formData.append('width', $("input[name='width']").val())
    formData.append('height', $("input[name='height']").val())
    formData.append('selected_image', file)
    $.ajax({
        url : "/.netlify/functions/resize-image",
        type: "POST",
        data: formData,
        cache:false,
        processData:false,
        contentType:false,
        success:function(response_data, status){
            Swal.fire({
                icon: 'success',
                showCloseButton: true,
                showCancelButton: false,
                text: "Image successfully resized!",
                focusConfirm: false,
                confirmButtonText:'Okay!',
                confirmButtonAriaLabel: 'Thumbs up, great!',
            })
            const data = JSON.parse(response_data)
            $("#output_image").attr("src", "data:image/png;base64," + data.output_image)
        },
        error: function(response_data, status){
            const error_message = JSON.parse(response_data.responseText)
            Swal.fire({
                icon: 'error',
                showCloseButton: true,
                showCancelButton: false,
                text: error_message.message,
                focusConfirm: false,
                confirmButtonText: 'Okay!',
                confirmButtonAriaLabel: 'Thumbs down, awful!',
            })

            document.getElementById("image_resizer_form").reset(); 
        }
    });
})