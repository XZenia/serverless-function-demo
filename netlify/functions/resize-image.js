import sharp from 'sharp'

const Busboy = require('busboy')

export const handler = async (event, context) => {
    const fields = await parseFormData(event)

    if (!fields.selected_image.filename.mimeType.includes("image")){
        return {
            statusCode: 403,
            body: JSON.stringify({
                message: "The file submitted is not a valid image!"
            })
        }
    }

    if (isNaN(parseInt(fields.width))){
        return {
            statusCode: 403,
            body: JSON.stringify({
                message: "Width is not a valid number!"
            })
        }
    }

    if (isNaN(parseInt(fields.height))){
        return {
            statusCode: 403,
            body: JSON.stringify({
                message: "Height is not a valid number!"
            })
        }
    }

    if (fields.width > 5000 || fields.height > 5000){
        return {
            statusCode: 403,
            body: JSON.stringify({
                message: "Width and height should not exceed 5000!"
            })
        }
    }

    const resized_image = await sharp(fields.selected_image.content)
    .resize({ width: parseInt(fields.width), height: parseInt(fields.height) })
    .toBuffer()

    return {
        statusCode: 200,
        body: JSON.stringify({
            output_image: resized_image.toString("base64")
        })
    }
}

function parseFormData(event){
    return new Promise((resolve) => {
        const fields = {};

        const busboy = Busboy({ headers: event.headers });

        // before parsing anything, we need to set up some handlers.
        // whenever busboy comes across a file ...
        busboy.on(
            "file",
            (fieldname, filestream, filename, transferEncoding, mimeType) => {
                // ... we take a look at the file's data ...
                filestream.on("data", (data) => {
                    // ... and write the file's name, type and content into `fields`.
                    fields[fieldname] = {
                        filename,
                        type: mimeType,
                        content: data,
                    };
                })
            }
        )

        busboy.on("field", (fieldName, value) => {
            // ... we write its value into `fields`.
            fields[fieldName] = value;
        })

        // once busboy is finished, we resolve the promise with the resulted fields.
        busboy.on("finish", () => {
            resolve(fields)
        })

        // now that all handlers are set up, we can finally start processing our request!
        busboy.end(Buffer.from(event.body, 'base64'));
    })
}
